FUNCTION_BLOCK Curve



// History:  
// v0.1 18 May 2016.  Jason Turner

//*****************************************************
//MIT License
//
//Copyright (c) 2016 Jason Turner https://www.turner2.com.au
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.
//******************************************************/


VAR_INPUT
    IN:         REAL;
    MAX_POINT:  INT := 10;  //Highest point number to use i.e. X_(MAX_POINT).
    X_00:       REAL := 0.0; 
    Y_00:       REAL := 0.0;
    X_01:       REAL := 10.0;
    Y_01:       REAL := 10.0;
    X_02:       REAL := 20.0;
    Y_02:       REAL := 20.0;
    X_03:       REAL := 30.0;
    Y_03:       REAL := 30.0;
    X_04:       REAL := 40.0;
    Y_04:       REAL := 40.0;
    X_05:       REAL := 50.0;
    Y_05:       REAL := 50.0;
    X_06:       REAL := 60.0;
    Y_06:       REAL := 60.0;
    X_07:       REAL := 70.0;
    Y_07:       REAL := 70.0;
    X_08:       REAL := 80.0;
    Y_08:       REAL := 80.0;
    X_09:       REAL := 90.0;
    Y_09:       REAL := 90.0;
    X_10:       REAL := 100.0;
    Y_10:       REAL := 100.0;
END_VAR

VAR_OUTPUT
    OUT:        REAL;
    ERR_CODE:  INT := 0;  // 0 = Healthy
END_VAR



VAR
    FoundFlag:       BOOL;
    MaxPoint:        INT;
    ii:              INT;
    Xv:         ARRAY[1..11] OF REAL;
    Yv:         ARRAY[1..11] OF REAL;
END_VAR

//reset error code
ERR_CODE := 0;
//reset Found flag
FoundFlag := 0;

//load Xv and Yv vectors
Xv[1] := X_00;
Xv[2] := X_01;
Xv[3] := X_02;
Xv[4] := X_03;
Xv[5] := X_04;
Xv[6] := X_05;
Xv[7] := X_06;
Xv[8] := X_07;
Xv[9] := X_08;
Xv[10] := X_09;
Xv[11] := X_10;

Yv[1] := Y_00;
Yv[2] := Y_01;
Yv[3] := Y_02;
Yv[4] := Y_03;
Yv[5] := Y_04;
Yv[6] := Y_05;
Yv[7] := Y_06;
Yv[8] := Y_07;
Yv[9] := Y_08;
Yv[10] := Y_09;
Yv[11] := Y_10;

//sanity-check inputs
//raise block error if x values are not monotonically increasing

MaxPoint := MAX_POINT;

IF (MaxPoint < 1) THEN
  MaxPoint := 1;
END_IF;

IF (MaxPoint > 10) THEN
  MaxPoint := 10;
END_IF;

FOR ii := 1 TO (MaxPoint) DO
	IF(ERR_CODE = 0) THEN
		IF( Xv[ii] >= Xv[ii + 1]) THEN
		  ERR_CODE := 1;                //ERR_CODE = 1:  Xv not in ascending order
		END_IF;
	END_IF;
END_FOR;
  
// determine where on the Xv scale IN sits

// re-use ii as "high end" index.
ii := 1;

IF (IN <= Xv[1] AND (ERR_CODE = 0) ) THEN
  ii := 1;
  OUT := Yv[1];
  FoundFlag := 1;
END_IF;

IF (NOT (FoundFlag) AND (IN >= Xv[MaxPoint + 1]) AND (ERR_CODE = 0) ) THEN
	ii := MaxPoint + 1;
	OUT := Yv[MaxPoint + 1];
	FoundFlag := 1;
END_IF;

//step through Xv, comparing IN with Xv[ii].
//stop when Xv[ii] is more than IN, or 
// index reaches last Xv.  In theory, we should never reach "index reaches last Xv" due to bounds checking above.
// but we still check.
WHILE ((ERR_CODE = 0) AND (NOT FoundFlag) AND (Xv[ii] <= IN ) AND (ii <= (MaxPoint + 1)) ) DO
	ii := ii + 1;
END_WHILE;

IF ( (NOT FoundFlag) AND (ii < 2) ) THEN 
	ERR_CODE := 2;  //strange mis-finding of data
END_IF;


IF(ERR_CODE = 0 AND FoundFlag = 0) THEN
	//if we got here error free and have not set OUT yet
	OUT := (Yv[ii] - Yv[ii - 1]) / (Xv[ii] - Xv[ii - 1]) * (IN - Xv[ii - 1]) + Yv[ii - 1];
	FoundFlag := 1;
END_IF;


