# Curve

Function to interpolate a piecewise linear curve.

Up to 11 points of the X and Y curve (10 segments) are configured. A live X value is entered via `IN`, and 
a Y value is calculated as `OUT`. If `IN` is less than the minimum, or greater than the maximum value of the range of 
configured X values, `OUT` is set to the corresponding enpoint value. i.e. the function does not extrapolate, but holds
the last curve value.

This function has the following input parameters (`n` ranges from `00` to `10`):

  * `IN: Real`: The value to be averaged.
  * `MAX_POINT: Int`: The highest point number to be used, i.e. X_MAX_POINT. Integer between 1 and 10.
       If outside this range, MAX_POINT will be constrained to this range.
  * `X_n: Real`: X co-ordinate of the `n`th vertex. Values must be in ascending order.
  * `Y_n: Real`: Y co-ordinate of the `n`th vertex.

The function has the following outputs:

  * `OUT: Real`: Interpolated output.
  * `ERR_CODE: Int`: Indicates misconfiguration.
    - `0`: No error.
	- `1`: `X_n` values not in ascending order.
	- `2`: Experimental feature.

## Tests and Expected Results

### Test 1: Error codes

Set the following. Note that `X_02` is less than `X_01`.

  * `MAX_POINT` = 2
  * `X_00` = 1.0
  * `Y_00` = 3.0
  * `X_01` = 2.0
  * `Y_01` = 1.0
  * `X_02` = 1.0
  * `Y_02` = 2.0
  * `IN` = 1.5
  
#### Result

`ERR_CODE` = 1. OUT does not change.

Set `IN` = 2.0. OUT does not change.


### Test 2: No extrapolation - high end.

Use Test 1 setup, with the following changes:

  * `MAX_POINT` = 2
  * `X_00` = 1.0
  * `Y_00` = 3.0
  * `X_01` = 2.0
  * `Y_01` = 1.0
  * _`X_02` = 3.0_
  * `Y_02` = 2.0
  * _`IN` = 4.0_
  
#### Result

`ERR_CODE` = 0. `OUT` = 2.0

### Test 3: No extrapolation - low end.

Use Test 2 setup, with the following changes:

  * `MAX_POINT` = 2
  * `X_00` = 1.0
  * `Y_00` = 3.0
  * `X_01` = 2.0
  * `Y_01` = 1.0
  * `X_02` = 3.0
  * `Y_02` = 2.0
  * _`IN` = -4.0_
  
#### Result

`ERR_CODE` = 0. `OUT` = 3.0
 
 ### Test 4: Interpolation
 
 Use Test 3 setup, with the following changes:

  * `MAX_POINT` = 2
  * `X_00` = 1.0
  * `Y_00` = 3.0
  * `X_01` = 2.0
  * `Y_01` = 1.0
  * `X_02` = 3.0
  * `Y_02` = 2.0
  * _`IN` = 1.5_
  
#### Result

`ERR_CODE` = 0. `OUT` = 2.0
 
