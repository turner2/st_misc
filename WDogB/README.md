# Watchdog - Binary

Watchdog that receives a toggling boolean, and sets or clears its own output boolean to match. 
The output boolean can be sent back to the source, daisy-chained to other watchdogs, or simply used
as a local memory store.

If the bit is used as a back-and-forth handshaking watchdog, the bit can be optionally inverted, i.e.
receive True, send False.

This function block has three input parameters:

  * `IN: Bool`: The toggling watchdog signal.
  * `INV_OUT: Bool`: Option to 

The outputs are:

  * `MAVG: Real`: The averaged signal.
  * `READY: Bool`: This is true when the number of samples taken equals or exceeds the moving average window
  length. e.g. for a 1-hour moving average, we have sampled at least an hour's worth of data. Before this,
  values for `MAVG` will still update, but the result may be treated with some suspicion.
  
Because of a lack of dynamic memory allocation, the window length is hard-coded in the variables

  * `max_n`: Number of samples to average. Set this to the number of samples you want.
  * `buckets`: The array where the samples are stored. Must be at least `max_n` elements long.
  
If you want some value other than the default, you'll need to edit these prior to compilation.

## Tests and Expected Results

Testing works better if the function block is slowed to 2-second execution so results could be observed. 

### Test 1: Averaging

Set `IN` to 20 and wait until `MAVG` = 20. Then set `IN` to 30. 

Expected Results:

  * Consistent step-size in changes in `MAVG` (0.5 per execution).
  * `MAVG` settles to same value as `IN`.

Set `IN` to -10
  
Expected Results:

  * Consistent step-size in changes in `MAVG` (2 per execution).
  * `MAVG` settles to same value as `IN`.

  
### Test 2: INIT

Set `INIT` to `True`

Expected Results:

  * `READY` changes from `True` to `False`.

Set `IN` to 10.

Expected Results:

  * `READY` remains `False`.
  * `MAVG` = 10, without delay.
  
Set `INIT` to `False`, then `IN` to 20.

Expected Results:

  * `READY` remains `False` for 20 cycles (40s).
  * `MAVG` ramps to 20 with uneven steps while `READY` is `False`.
  


## TODO

### Adjustable `max_n`

Make `buckets` large-ish, and make `max_n` an adjustable input (with bounds-checking 
to keep one from walking off the end of an array).