FUNCTION_BLOCK fbApcDiff
// function to check for changes in any of the following operAPC values:
// * setMag
// * trigger
// upon detecting a change, the following happens:
// * .trigger is set on the output.
// * DIFF_FLAG is set true.
// because values are sent on a rising-edge of .trigger,
// changes will not be checked for if .trigger is true; 
// the block needs at least one scan to drop .trigger back
// to 0.
// Revision history:
// 2020-05-20 - Rev 1 Jason Turner
// 2018-12-16 - Rev 0 Jason Turner
VAR
	FirstScan: BOOL := TRUE;
END_VAR
VAR_INPUT
	IN: APC;  // source Analog Output tag.
	DB: REAL := 0.1;  // Deadband - setpoint changes bigger than DB cause .trigger to be set for 1 scan.
	BLOCK_DIFF: BOOL := FALSE; // TRUE = disable difference-based setting of .trigger
END_VAR

VAR_OUTPUT
	OUT: APC;  // Destination Analog Output Tag
	DIFF_FLAG: BOOL;  // TRUE if OUT has been updated.
END_VAR
