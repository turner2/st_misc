# Watchdog - Integer

Watchdog that receives a changing integer, and sets an alarm bit when the integer has not changed for
a pre-set time. 

This function block has two input parameters:

  * `IN: DInt`: The changing integer.
  * `TIMOUT: TIME`: pre-set time before alarm bit is set when `IN` does not change.

The block outputs are:

  * `ALARM: BOOL`: True if `IN` has not changed for longer than `TIMEOUT`.
  * `RESP_TIME: TIME`: Time since last change of `IN`.
  * `LastIn: DINT`: Last value recorded of `IN`.


## Tests and Expected Results

