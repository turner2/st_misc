TITLE = 'Transfer'
//VERSION : '0.1.2'
//AUTHOR  : 'Jason Turner jason@turner2.com.au'
//
// Bumplessly transfer from one REAL input to the other.
// History:
// v0.1.2 10 Aug 2017.  Jason Turner.  Cosmetic changes, comments, SEL compatability.  
// v0.1.1 25 Jun 2016.  Jason Turner.  Bug fix so that switching when SwitchDelta already > 0 is still bumpless.
// v0.1.0 13 Jun 2016.  Jason Turner.  Initial version.  Switch bumplessly between two REAL values.
//
//
//*****************************************************
//MIT License
//
//Copyright (c) 2016 Jason Turner https://www.turner2.com.au
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.
//******************************************************/

VAR_INPUT
    IN_1:           REAL;       //First input
    IN_2:           REAL;       //Second input.
    
    //
    SWITCH:         BOOL := False;     //False=Use input 1, True=Use input 2.
	BAL_RATE1_2:	REAL := 0.0;	// Max rate-of-change per scan on switch from 1 to 2. Ignored if zero.
    BAL_RATE2_1:    REAL := 0.0;   //Max rate-of-change per scan on switch from 2 to 1.  Ignored if zero.
END_VAR


VAR_OUTPUT
    OUT:            REAL;    //Process Value
END_VAR

VAR

    PV_Calc:        REAL; //Raw calculated PV.
    
    PrevSwitch:     BOOL := 0;
    SwitchDelta:    REAL := 0.0; //Difference after switching status of inputs between PV and Calculted PV.
    
    TinyReal:       REAL := 1e-12; //approximate zero.
    FirstScan:      BOOL := 1;   //block's first scan.  Don't process bumpless switching on first scan.
	Bal_Rate:		REAL;
END_VAR 

BEGIN
    // Detect switching
    IF( (NOT FirstScan) AND (PrevSwitch <> SWITCH)) THEN
        IF(NOT SWITCH) THEN  //switching to input 1
            SwitchDelta := OUT - IN_1;
        ELSE  //switching to input 2
            SwitchDelta := OUT - IN_2;
        END_IF;
        
    END_IF;
	//select balance rate-of-change
	IF (SWITCH) THEN
		Bal_Rate := BAL_RATE1_2;
	ELSE
		Bal_Rate := BAL_RATE2_1;
	END_IF;
    
    IF(NOT SWITCH) THEN
        PV_Calc := IN_1;
    ELSE
        PV_Calc := IN_2;
    END_IF;
    
    IF (ABS(SwitchDelta) > Bal_Rate AND (Bal_Rate > TinyReal)) THEN  //still some ramping to be done.    
        IF (SwitchDelta >= 0.0) THEN //Positive Switchdelta:  PV > PV_Calc
            SwitchDelta := SwitchDelta - Bal_Rate;
        END_IF;
        IF(SwitchDelta < 0.0) THEN //Negative Switchdelta
            SwitchDelta := SwitchDelta + Bal_Rate;
        END_IF;
        PV_Calc := PV_Calc + SwitchDelta;
        
        OUT := PV_Calc;
    ELSE //SwitchDelta <= Bal_Rate; last scan of switching
        OUT := PV_Calc;
        SwitchDelta := 0.0;
    END_IF;
    
    
    //last lines of block.    
    PrevSwitch := SWITCH;
    FirstScan := 0;
END_FUNCTION_BLOCK
