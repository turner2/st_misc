# Transfer

Switch between two `Real` values, optionally bumplessly.

## Inputs

(`n` = 1 or 2):

  * `IN_n: Real`: nth input to be selected from. 
  * `SWITCH: Bool`: If `False`, select `IN_1`. If `True`, select `IN_2`.
  * `BAL_RATE1_2: Real`: Maximum rate-of-change per scan on switch from 1 to 2. Ignored if zero.
  * `BAL_RATE2_1: Real`: Maximum rate-of-change per scan on switch from 2 to 1. Ignored if zero.
  
Note that the rate-of-change limit imposed by `BAL_RATE*` only affects the rate of change of the switch.
If the inputs are changing faster, this will be correctly reflected in the block output. See 'Balance Rate Example'
below.

## Outputs 

  * `OUT: Real`: Selected output.
  
## Details

### Selection

The `TRANSFER` block takes two `Real` inputs, and passes one to the `OUT` parameter. The switch can be instantaneous, 
or ramp slowly between the inputs.

### Balance Rate Examples / Tests

#### Simple Case

Setup:

  * `IN_1`: Constant, 10.0
  * `IN_2`: Constant, 20.0
  * `BAL_RATE1_2`: 0.1
  * `BAL_RATE2_1`: 0.0
  
 1) Start with `SWITCH=False`, `OUT` is 10.0.
 2) Set `SWITCH=True`. 

`OUT` ramps from 10.0 to 20.0 at 0.1 / scan.

  3) Set `SWITCH=False`.
  
`OUT` switches instantly to 10.0.

#### Dynamic Case

Setup:

  * `IN_1`: Start at 10.0, ramping up at 0.5 / scan
  * `IN_2`: Start at 0.0, ramping up at 0.5 / scan.
  * `BAL_RATE1_2`: 0.1
  * `BAL_RATE2_1`: 0.1

 1) Start with `SWITCH=False`, `OUT` tracks `IN_1`.
 2) Set `SWITCH=True`.

`OUT` changes at the same rate as `IN_2`, and ramps towards `IN_2` at 0.1 / scan. In other words,
`OUT` ramps up at 0.4 / scan.

 3) Set `SWITCH=False`.

`OUT` changes at the same rate as `IN_1`, and ramps towards `IN_1` at 0.1 / scan. In other words,
`OUT` ramps at 0.6 / scan.


These cases can be tweaked.