FUNCTION_BLOCK Median16
//TITLE = 'Median16'
//VERSION : '0.1.0'
//AUTHOR  : 'Jason Turner jason@turner2.com.au'
//
// Median-select of up to 16 elements.
// Yes, that's bubble sort in the code. Since we'll never get more than 16 
// elements to search, it was easy to implement and maintain, 
// with a tiny performance hit. In my opinion: cycles well spent.
//
//
// History:  
// v0.1 27 Jun 2016.  Jason Turner - initial release.

//*****************************************************
//MIT License
//
//Copyright (c) 2016 Jason Turner https://www.turner2.com.au
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.
//******************************************************/



VAR_INPUT
    IN_1:       REAL;       //Input 1.
    USE_1:      BOOL := 0;  //1=use input 1.
    IN_2:       REAL;       //Input 2.
    USE_2:      BOOL := 0;  //1=use input 2.
    IN_3:       REAL;       //Input 3.
    USE_3:      BOOL := 0;  //1=use input 3.
    IN_4:       REAL;       //Input 4.
    USE_4:      BOOL := 0;  //1=use input 4.
    IN_5:       REAL;       //Input 5.
    USE_5:      BOOL := 0;  //1=use input 5.
    IN_6:       REAL;       //Input 6.
    USE_6:      BOOL := 0;  //1=use input 6.
    IN_7:       REAL;       //Input 7.
    USE_7:      BOOL := 0;  //1=use input 7.
    IN_8:       REAL;       //Input 8.
    USE_8:      BOOL := 0;  //1=use input 8.
    IN_9:       REAL;       //Input 9.
    USE_9:      BOOL := 0;  //1=use input 9.
    IN_10:      REAL;       //Input 10.
    USE_10:     BOOL := 0;  //1=use input 10.
    IN_11:      REAL;       //Input 11.
    USE_11:     BOOL := 0;  //1=use input 11.
    IN_12:      REAL;       //Input 12.
    USE_12:     BOOL := 0;  //1=use input 12.
    IN_13:      REAL;       //Input 13.
    USE_13:     BOOL := 0;  //1=use input 13.
    IN_14:      REAL;       //Input 14.
    USE_14:     BOOL := 0;  //1=use input 14.
    IN_15:      REAL;       //Input 15.
    USE_15:     BOOL := 0;  //1=use input 15.
    IN_16:      REAL;       //Input 16.
    USE_16:     BOOL := 0;  //1=use input 16.
END_VAR
VAR_OUTPUT
    MEDIAN:     REAL;
    N:          INT;
END_VAR

VAR
    Inputs:             ARRAY[1..16] OF REAL; //block inputs in vector form
    Use:                ARRAY[1..16] OF BOOL; //block inputs in vector form
    WorkingInputs:      ARRAY[1..16] OF REAL; //block inputs packed (i.e. no gaps)
    ii:                 INT;
    jj:                 INT;
    kk:                 INT;
    minIndex:           INT;
    minValue:           REAL;
    swapValue:          REAL;
    NisOdd:             BOOL;
END_VAR

BEGIN
//Populate arrays
Inputs[1] := IN_1;
Inputs[2] := IN_2;
Inputs[3] := IN_3;
Inputs[4] := IN_4;
Inputs[5] := IN_5;
Inputs[6] := IN_6;
Inputs[7] := IN_7;
Inputs[8] := IN_8;
Inputs[9] := IN_9;
Inputs[10] := IN_10;
Inputs[11] := IN_11;
Inputs[12] := IN_12;
Inputs[13] := IN_13;
Inputs[14] := IN_14;
Inputs[15] := IN_15;
Inputs[16] := IN_16;

Use[1] := USE_1;
Use[2] := USE_2;
Use[3] := USE_3;
Use[4] := USE_4;
Use[5] := USE_5;
Use[6] := USE_6;
Use[7] := USE_7;
Use[8] := USE_8;
Use[9] := USE_9;
Use[10] := USE_10;
Use[11] := USE_11;
Use[12] := USE_12;
Use[13] := USE_13;
Use[14] := USE_14;
Use[15] := USE_15;
Use[16] := USE_16;

//zero the WorkingInputs array
FOR ii := 1 TO 16 DO
	WorkingInputs[ii] := 0.0;
END_FOR;

//how many useful inputs do we have?
N := 0;
jj := 1;
FOR ii := 1 TO 16 DO
	IF(Use[ii]) THEN
		WorkingInputs[jj] := Inputs[ii];
		jj := jj + 1;
		N := N + 1;
	END_IF;
END_FOR;

//-----------------trivial cases--------------------------------
IF(N = 1) THEN
	MEDIAN := WorkingInputs[1];
END_IF;
IF(N = 2) THEN 
	MEDIAN := (WorkingInputs[1] + WorkingInputs[2])/2;
END_IF;
//-----------------end trivial cases--------------------------------

//non-trivial cases
//
//1. odd or even N?
IF( (N MOD 2) = 0) THEN //even
	NisOdd := 0;
ELSE 
	NisOdd := 1;
END_IF;

// kk is the median position (or greater of the two middle values position for even numbers)
kk := REAL_TO_INT(CEILING(N/2)) + 1;

IF(N > 2) THEN
	FOR ii := 1 TO kk DO
		minIndex := ii;
		minValue := WorkingInputs[ii];
		FOR jj := (ii + 1) TO N DO
			IF(WorkingInputs[jj] < minValue) THEN
				minIndex := jj;
				minValue := WorkingInputs[jj];
				swapValue := WorkingInputs[ii];
				WorkingInputs[ii] := WorkingInputs[minIndex];
				WorkingInputs[minIndex] := swapValue;
			END_IF; //(WorkingInputs[jj] < minValue)
			//swap values
			
		END_FOR; //jj
	END_FOR; //ii
END_IF;
//WorkingValues first kk element should now be sorted in increasing order.
IF(NisOdd) THEN
	MEDIAN := WorkingInputs[kk];
ELSE
	MEDIAN := (WorkingInputs[kk] + WorkingInputs[kk - 1]) / 2;
END_IF;

END_FUNCTION_BLOCK
    
