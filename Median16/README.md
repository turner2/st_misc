# MEDIAN16

Function block to find the median of up to 16 `REAL` inputs. For clarity: the median is defined as the middle-most valued
data point when you have an odd number of data points. When you have an even number of data points, the median is the mean
of the two middle-most values. Finding the "middlest" is done by sorting.

This function has the following input parameters (`n` ranges from `1` to `16`):

* `IN_n`: Real-valued input.
* `USE_n`: Boolean. Should this input be used. Defaults to `FALSE`. This can be set at compile time, or connected
and used during run time, e.g. to reject signals with bad quality.

The block has the following outputs:

* `MEDIAN`: Real-valued median.
* `N`: Number of inputs selected. This can be used to indicate total bad quality (all inputs bad quality).
