FUNCTION_BLOCK sun_calc_fb
// calculate sunrise or sunset times, given date, lat, lon, and utc offset
// algorithm does not correct for atmospheric refraction
// 
// depends: "floor" function.

(* MIT License
Copyright (c) 2020 Jason Turner https://www.turner2.com.au
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.*)

VAR_OUTPUT
	sun_time: REAL; // sunrise or sunset in decimal hours utc+offset, e.g. 05:45 will be represented as 5.75
	sun_hour: DINT; // sunrise or sunet hour as dint utc+offset
	sun_minute: DINT; // sunrise or sunset minutes as dint utc+offset
END_VAR
VAR_INPUT
	YEAR: REAL := 2021;
	MONTH: REAL := 1; //jan = 1
	DAY: REAL := 1;   // day-of-month
	sunrise: BOOL := TRUE; // 0=sunset, 1=sunrise
	offset: REAL := 10; // utc offset
	lat: REAL := -33.865;
	long: REAL :=  151.209444;     // sydney lat and long.
END_VAR
VAR
	wyear: REAL;
	doyN1: REAL;
	doyN2: REAL;
	doyN3: REAL;
	day_of_year : REAL;
	zenith: REAL :=  90.83333333333333;
	deg2rad: REAL := 0.01745329251994;  // pi/180
	rad2deg: REAL := 57.29577951;       // 180/pi
	
	longHour: REAL;   //longetude hour
	solar_t: REAL; //solar time
	//local_t: REAL; //sunrise time in local solar time
	sol_mean_anom: REAL; //solar mean anomaly
	sol_true_lon: REAL; // true solar longetude
	RA: REAL;  //right ascension
	Lquadrant: REAL;
	RAquadrant: REAL;
	sinDec: REAL; //declination 
	cosDec: REAL; //declination
	cosH: REAL; //
	H: REAL;  // sun's local solar angle
	sun_time_loc_mean: REAL;  // local mean time sunrise
	sun_time_utc: REAL; //sunrise time in UTC
	
	//local_time: REAL; // sunrize in local time zone 
END_VAR




// algorithm ported from c++ and uses struct tm. 
// year since 1900
//working year
wyear := year - 1900.0;
//calculate day of year
doyN1 := floor(275 * MONTH/9);
doyN2 := floor((MONTH + 9) / 12);
doyN3 := (1 + floor((wyear - 4 * floor(wyear / 4) + 2) / 3));
day_of_year := doyN1 - (doyN2 * doyN3) + DAY - 30;

// convert longetude to hour value.
longHour := long / 15;
IF sunrise THEN
	solar_t := day_of_year + ((6 - longHour)/24);
ELSE
	solar_t := day_of_year + ((18 - longHour)/24);
END_IF
// solar mean anomaly
sol_mean_anom := (0.9856 * solar_t) - 3.289;

// true solar longetude
sol_true_lon := sol_mean_anom + (1.916 * SIN(sol_mean_anom * deg2rad)) + (0.020 * SIN(2 * sol_mean_anom * deg2rad)) + 282.634;
IF (sol_true_lon > 360) THEN
	sol_true_lon := sol_true_lon - 360;
ELSIF ( sol_true_lon < 0) THEN
	sol_true_lon := sol_true_lon + 360;
END_IF

 //calculate the Sun's right ascension
RA := rad2deg * ATAN(0.91764 * TAN(sol_true_lon * deg2rad));
IF (RA > 360) THEN
	RA := RA - 360;
ELSIF (RA < 0) THEN
	RA := RA + 360;
END_IF;


// right ascension
Lquadrant := (floor(sol_true_lon / (90))) * 90;
RAquadrant := (floor(RA / 90)) * 90;
RA := RA + (Lquadrant - RAquadrant);

//convert right ascension to hours
RA := RA / 15;

// sun's declination
sinDec := 0.39782 * SIN(sol_true_lon * deg2rad);
cosDec := COS(ASIN(sinDec));

// sun's local hour angle
cosH := (COS(zenith * deg2rad) - (sinDec * SIN(lat* deg2rad))) / (cosDec * COS(lat * deg2rad));
IF sunrise THEN
	H := 360 - rad2deg * ACOS(cosH);
ELSE
	H := rad2deg * ACOS(cosH);
END_IF
H := H / 15;

//local mean sunrise time
// note that "solar local time" is not "local time zone time".
// "solar local time" can be inferred from longetude, but time zones
// are part astronomical and part political.
// UTC however is solar, and converting to UTC from local solar time is simple.
// Converting from UTC to local is done using the "offset" input.
sun_time_loc_mean := H + RA - (0.06571 * solar_t) - 6.622;

// convert to UTC
sun_time_utc := sun_time_loc_mean - longHour;
IF (sun_time_utc > 24) THEN
	sun_time_utc := sun_time_utc - 24;
ELSIF (sun_time_utc < 0) THEN
	sun_time_utc := sun_time_utc + 24;
END_IF

sun_time := sun_time_utc + offset; // 
IF (sun_time > 24) THEN
	sun_time := sun_time - 24;
ELSIF (sun_time < 0) THEN
	sun_time := sun_time + 24;
END_IF

sun_hour := REAL_TO_DINT(floor(sun_time));
sun_minute := REAL_TO_DINT( (sun_time - DINT_TO_REAL(sun_hour)) * 60);
