# Signal Select

This block takes three `Real` inputs, and performs selection between them. Possible selections are:

  * Primary, Secondary, Tertiary (i.e. input 1 unless Bad Quality or De-Selected, then input 2 unless ... )  
  * Minimum
  * Median
  * Maximum
  
  
Values with Bad Quality can be rejected. Values can also be de-selected regardless their quality status, e.g. instrument 
being calibrated.

## Inputs 

(`n` = 1,2, or 3):

  * `IN_n: Real`: nth input to be selected from. 
  * `USE_n: Bool`: Use the nth input. e.g. If `IN_3` is not connected, set `USE_3` to `False`. This avoids selection bugs,
    and allows the block to indicate the correct quality.
  * `BAD_IN_n: Bool`: Flag to indicate that input `n` has Bad Quality.
  * `MAN_DESEL_n`: Manual (typically from SCADA) de-selection of input `n`.
  * `MUL_n`: Scaling. Multiply `IN_n` by `MUL_n` before processing.
  * `ADD_n`: Bias term. Add `ADD_n` to `IN_n` before processing.
  * `BAL_RATE: Real`: When an input's quality changes, or the input is manually (de) selected for service, 
    do not switch suddenly to the new selection. Ramp to the new value at `BAL_RATE` per block cycle. If `MUL_n`
	is/are used, BAL_RATE rate-of-change is in PV scale, not `IN_n` scale.
	Ignored if `BAL_RATE` is zero.
  * `SEL_MODE: Int`: Algoritm used to select between signals. 
     
	 - `SEL_MODE` = 0: Primary, Secondary, Tertiary.
	 - `SEL_MODE` = 1: Minimum Select.
	 - `SEL_MODE` = 2: Median Select
	 - `SEL_MODE` = 3: Maximum Select.
	
	2 is the default value, and will be used if `SEL_MODE` is not specified, or if a value outside the range 0 to 3 is 
	used.

## Outputs

(`n` = 1,2, or 3):

  * `PV: Real`: Result of selection.
  * `BAD_QUALITY: Bool`:  The block has no selectable inputs; `PV` will remain at last good value.
  * `BAD_WARN: Bool`: The block has at least one bad quality or de-selected input.
  * `OOS_n: Bool`: Flag to indicate that input `n` has bad quality or is de-selected. `True` if `USE_n` is
    `True` and either `BAD_IN_n` or `MAN_DESEL_n` is `True`.
  * `STATUS: Int`: Packed integer indicating the status of the inputs. Bits used are:
    
	- 0x0001: `OOS_1` is `True`.
	- 0x0002: `OOS_2` is `True`.
	- 0x0004: `OOS_3` is `True`.
	- 0x0010: `USE_1` is `True`.
	- 0x0020: `USE_2` is `True`.
	- 0x0040: `USE_3` is `True`.
    
	For example, if the block is configured to use inputs 1 and 2 only (`USE_3 = False`), and input 1 is
	Bad Quality or de-selected, `STATUS` will be: 0x0031. In binary: `0000 0000 0011 0001`.
  * `MODE: String`: Text representation of what selection mode is being used. It will be one of:
  
    - `1,2,3`
	- `Min`
	- `Median`
	- `Max`
  
## Details

### Signal Availability
For all selection modes, `IN_n` is available for 
selection if: `USE_n` is `True`, `BAD_IN_n` is `False`, and `MAN_DESEL_n` is `False`.

If no inputs are available, the output `BAD_QUALITY` is set, and `PV` will not update.

If any inputs have `USE_n` and `OOS_n` both `True`, `BAD_WARN` will be true.  

### Bumpless Transfer

When `OOS_n` or `USE_n` changes (i.e. when `STATUS` changes), 
`PV` will not abruptly change if the change causes a new input to be selected. Instead, the switch from the 
current selection to the new selection will ramp at `BAL_RATE` each block scan. Note that this only applies 
to changes of `USE_n`, `BAD_IN_n`, or `MAN_DESEL_n`. Changes due to e.g. `IN_2` jumping and becoming greater than
`IN_1` when the block is in `SEL_MODE` 3 (maximum) happen with no ramping. This feature is not used if `BAL_RATE` is 
zero.

### Selection Criteria

The block takes all available inputs `IN_n`, scales them, then applies the selection mode configured in `SEL_MODE`. 

#### Scaling

Each input is scaled: `IN_n * MUL_n + ADD_n` prior to selection. This can be useful in the case of 
instrument calibration differences, or differing units between inputs, e.g. one wind-speed meter in m/s, and 
another in km/h.

#### `SEL_MODE = 0` Primary, Secondary, Tertiary

Also called 1,2,3 or Primary, Backup, Backup-backup.

 * Input 1 is selected if available. 
 * If Input 1 is not available, Input 2 is used if availabe.
 * If Inputs 1 and 2 are not available, Input 3 is used if available.

#### `SEL_MODE = 1` Minimum Select

The minimum of all available inputs is selected.

#### `SEL_MODE = 2` Median Select

The median of all availabe inputs is selected.

Note that 
the median of two signals is the same as their mean value. The definition of median for any even number of data
points is the mean of the two middle-most points.

#### `SEL_MODE = 3` Maximum Select

The maximum of all available inputs is selected.


## Tests and Expected Results
