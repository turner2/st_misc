FUNCTION_BLOCK SIGNAL_SELECT
VAR
END_VAR
//TITLE = 'Signal Select'
//VERSION : '0.2.0'
//AUTHOR : 'Jason Turner jason@turner2.com.au'
//PLATFORM : SEL RTAC 
// History:  
// v0.2 19 Jan 2018   Jason Turner 
//                  - SEL Port
// v0.1 31 Oct 2015.  Jason Turner - initial release.
//                  - Low, Median, or High Select.
//                  - Bad inputs are ignored.  When inputs become good, they continue
//                    to be ignored for a time delay period.
//                  - Inputs can be manually de-selected (e.g. taken offline for maintenance)
//                  - Bias and scale can be applied to each input.
//      

//*****************************************************
//MIT License
//
//Copyright (c) 2016 Jason Turner https://www.turner2.com.au
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.
//******************************************************/


VAR_INPUT


    USE_1:          BOOL := 0;     //Use input 1.  Set at config only.
    USE_2:          BOOL := 0;     //Use input 2.  Set at config only.
    USE_3:          BOOL := 0;     //Use input 3.  Set at config only.

    //
    IN_1 :          REAL;          //Input 1
    BAD_IN_1:       BOOL := 0;     //Boolean flag - bad quality input 1. 1=Bad Quality.
    MAN_DESEL_1:    BOOL := 0;     //Manual deselect of input 1.
        

    IN_2 :          REAL;          //Input 2
    BAD_IN_2:       BOOL := 0;     //Boolean flag - bad quality input 2. 1=Bad Quality.
    MAN_DESEL_2:    BOOL := 0;     //Manual deselect of input 2.


    IN_3 :          REAL;          //Input 3
    
    BAD_IN_3:       BOOL := 0;     //Boolean flag - bad quality input 3. 1=Bad Quality.
    MAN_DESEL_3:    BOOL := 0;     //Manual deselect of input 3.
                                          
    BAL_RATE:   REAL;            //Max rate-of-change per scan on switch, fault, sel/desel etc.  Ignored if zero or negative.
    
    MUL_1:     REAL := 1.0;       //Multiply value by this amount before processing.
    ADD_1:     REAL := 0.0;       //Add to value before processing.

    MUL_2:     REAL := 1.0;       //Multiply value by this amount before processing.
    ADD_2:     REAL := 0.0;       //Add to value before processing.

    MUL_3:     REAL := 1.0;       //Multiply value by this amount before processing.
    ADD_3:     REAL := 0.0;       //Add to value before processing.

                
    SEL_MODE:  INT := 2;          //Select mode:  0 = 1,2,3, 1 = Min, 2 = Median, 3 = Max.
    
                  
END_VAR


VAR_OUTPUT
    PV:             REAL;    //Process Value, in Engineering Units.
    BAD_QUALITY:    BOOL;    //Bad Quality alarm signal (all used inputs bad or all deselected).
    BAD_WARN:       BOOL;    //Bad Quality warning (1oo2 or 2oo3).
    OOS_1:          BOOL;    //Input 1 Out Of Service.
    OOS_2:          BOOL;    //Input 2 Out Of Service.
    OOS_3:          BOOL;    //Input 3 Out Of Service.
    STATUS:         WORD;    //Block I/O status bits.
    MODE:           STRING;  //Text description of current mode.
END_VAR

VAR

    PV_Calc:        REAL; //Raw calculated PV.
    PV_Min:         REAL;  
    PV_Max:         REAL;
   

    SwitchDelta:    REAL := 0.0; //Difference after switching status of inputs between PV and Calculted PV.
    
    TinyReal:       REAL := 1e-12; //approximate zero.
    
    
    LastStatus:     WORD := 16#0000;
    LastIn1:        REAL;    
    LastIn2:        REAL;
    LastIn3:        REAL;
    LastDesel1:     BOOL := 0;
    LastDesel2:     BOOL := 0;
    LastDesel3:     BOOL := 0;
	
	OutOfService_1: WORD  :=  16#0001;
    OutOfService_2: WORD  :=  16#0002;
    OutOfService_3: WORD  :=  16#0004; 
    
    UseInput_1: 	WORD   := 16#0010;
    UseInput_2:		WORD   := 16#0020;   
    UseInput_3:		WORD   := 16#0040;
   
END_VAR 


STATUS := 16#0000;  //remove all status bits

//unset manual de-selection if another input is already de-selected.
IF ((MAN_DESEL_1 AND NOT LastDesel1) AND (OOS_2 OR OOS_3)) THEN
	MAN_DESEL_1 := 0;
END_IF;

IF ((MAN_DESEL_2 AND NOT LastDesel2) AND (OOS_1 OR OOS_3)) THEN
	MAN_DESEL_2 := 0;
END_IF;

IF ((MAN_DESEL_3 AND NOT LastDesel3) AND (OOS_1 OR OOS_2)) THEN
	MAN_DESEL_3 := 0;
END_IF;

//bad or deselected input detection.
	
OOS_1 := (MAN_DESEL_1) OR (BAD_IN_1);
OOS_2 := (MAN_DESEL_2) OR (BAD_IN_2);
OOS_3 := (MAN_DESEL_3) OR (BAD_IN_3);
	
//There's probably a simpler way to do this using bitwise logic
	
IF(OOS_1) THEN
	STATUS := STATUS OR OutOfService_1;
END_IF;

IF(OOS_2) THEN
	STATUS := STATUS OR OutOfService_2;
END_IF;   

IF(OOS_3) THEN
	STATUS := STATUS OR OutOfService_3;
END_IF;   

IF(USE_1) THEN
	STATUS := STATUS OR UseInput_1;
END_IF;

IF(USE_2) THEN
	STATUS := STATUS OR UseInput_2;
END_IF;

IF(USE_3) THEN
	STATUS := STATUS OR UseInput_3;
END_IF;


//Processing

//Sanity check SEL_MODE, output MODE
CASE SEL_MODE OF
	0 : MODE := '1,2,3';
	1 : MODE := 'Min';
	2 : MODE := 'Median';
	3 : MODE := 'Max';
ELSE
	SEL_MODE := 2;  //default mode.
	MODE := 'Med';
END_CASE;

   
//apply scaling to inputs
LastIn1 := IN_1;
IN_1 := IN_1 * MUL_1 + ADD_1;

LastIn2 := IN_2;
IN_2 := IN_2 * MUL_2 + ADD_2;

LastIn3 := IN_3;
IN_3 := IN_3 * MUL_3 + ADD_3;

IF(STATUS <> LastStatus) THEN
	SwitchDelta := PV - PV_Calc;
END_IF;

//Case 1: 3 inputs, all good.
IF (USE_1 AND (NOT OOS_1) AND USE_2 AND (NOT OOS_2) AND USE_3 AND (NOT OOS_3)) THEN
	
	PV_Min := MIN(IN_1, MIN(IN_2, IN_3));
	PV_Max := MAX(IN_1, MAX(IN_2, IN_3));
	
	CASE SEL_MODE OF
		0 : PV_Calc := IN_1;
		1 : PV_Calc := PV_Min;
		2 : PV_Calc := IN_1 + IN_2 + IN_3 - PV_Max - PV_Min;
		3 : PV_Calc := PV_Max;         
	END_CASE;
END_IF;

//Case 2:  2 inputs used.
//Case 2a: inputs 1 and 2 only in service.

IF (USE_1 AND (NOT OOS_1) AND USE_2 AND (NOT OOS_2) AND ((NOT USE_3) OR OOS_3)) THEN
	
	PV_Min := MIN(IN_1, IN_2);
	PV_Max := MAX(IN_1, IN_2);
	
	CASE SEL_MODE OF
		0 : PV_Calc := IN_1;
		1 : PV_Calc := PV_Min;
		2 : PV_Calc := (IN_1 + IN_2)/2.0;
		3 : PV_Calc := PV_Max;         
	END_CASE;
	
END_IF;

//Case 2b:  inputs 1 and 3 only in service.

IF (USE_1 AND (NOT OOS_1) AND ((NOT USE_2) OR OOS_2) AND USE_3 AND (NOT OOS_3)) THEN
	
	PV_Min := MIN(IN_1, IN_3);
	PV_Max := MAX(IN_1, IN_3);
	
	CASE SEL_MODE OF
		0 : PV_Calc := IN_1;
		1 : PV_Calc := PV_Min;
		2 : PV_Calc := (IN_1 + IN_3)/2.0;
		3 : PV_Calc := PV_Max;         
	END_CASE;
	
END_IF;

//Case 2c:  inputs 2 and 3 only in service.

IF ((NOT USE_1 OR OOS_1) AND USE_2 AND (NOT OOS_2) AND USE_3 AND (NOT OOS_3)) THEN
	
	PV_Min := MIN(IN_2, IN_3);
	PV_Max := MAX(IN_2, IN_3);
	
	CASE SEL_MODE OF
		0 : PV_Calc := IN_2;
		1 : PV_Calc := PV_Min;
		2 : PV_Calc := (IN_2 + IN_3)/2.0;
		3 : PV_Calc := PV_Max;         
	END_CASE;        
END_IF;

//Case 3:  one input active

//Case 3a:  input 1 only in service
IF (USE_1 AND (NOT OOS_1) AND ((NOT USE_2) OR OOS_2) AND ((NOT USE_3) OR OOS_3)) THEN
	PV_Calc := IN_1;
END_IF;

//Case 3b:  input 2 only in service
IF ( ((NOT USE_1) OR OOS_1) AND USE_2 AND (NOT OOS_2) AND ((NOT USE_3) OR OOS_3)) THEN
	PV_Calc := IN_2;
END_IF;

//Case 3c:  input 3 only in service
IF ( ((NOT USE_1) OR OOS_1) AND ((NOT USE_2) OR OOS_2) AND USE_3 AND (NOT OOS_3)) THEN
	PV_Calc := IN_3;
END_IF;


BAD_WARN := (OOS_1 AND USE_1) OR (OOS_2 AND USE_2) OR (OOS_3 AND USE_3);
BAD_QUALITY := (OOS_1 OR NOT USE_1) AND (OOS_2 OR NOT USE_2) AND (OOS_3 OR NOT USE_3);


IF(STATUS <> LastStatus) THEN
	SwitchDelta := PV - PV_Calc;
END_IF;
LastStatus := STATUS;

IF (NOT BAD_QUALITY) THEN  //pv does not update on bad quality.
	//check if ramping is needed.
	IF (ABS(SwitchDelta) > BAL_RATE AND (BAL_RATE > TinyReal)) THEN  //still some ramping to be done.
			PV := PV_Calc + SwitchDelta;
		IF (SwitchDelta >= 0.0) THEN //Positive Switchdelta:  PV > PV_Calc
			SwitchDelta := SwitchDelta - BAL_RATE;
		END_IF;
		IF(SwitchDelta < 0.0) THEN //Negative Switchdelta
			SwitchDelta := SwitchDelta + BAL_RATE;
		END_IF;
	ELSE //SwitchDelta <= BAL_RATE
		PV := PV_Calc;
		SwitchDelta := 0.0;
	END_IF;
END_IF;

//Reset scaling, to prevent accumulation on manual inputs
IN_1 := LastIn1;
IN_2 := LastIn2;
IN_3 := LastIn3; 

LastDesel1 := MAN_DESEL_1;
LastDesel2 := MAN_DESEL_2;
LastDesel3 := MAN_DESEL_3;
