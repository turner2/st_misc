# RateLim

Simple block to limit per-scan rate-of-change of the output.
Inputs are:

* `IN`: Real. Value to be rate-limited.
* `UP_LIM`: Real. Per-scan change positive change allowed of rate-limited value.
* `DN_LIM`: Real. Per-scan change negative change allowed of rate-limited value.
* `PASS_THRU`: Boolean. If true, no rate limiting is done. `IN` is copied directly to `OUT`.

Output:

`OUT`: Real. The rate-limited value.
