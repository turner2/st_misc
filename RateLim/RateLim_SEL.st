FUNCTION_BLOCK RateLim
// History:  
// v0.2 5 Dec 2016.   Jason Turner - simplified increment statements.
// v0.1 17 Aug 2016.  Jason Turner - initial release.
//*****************************************************
//MIT License
//
//Copyright (c) 2016 Jason Turner https://www.turner2.com.au
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.
//******************************************************/



VAR_INPUT    
    IN :        REAL;           //Input to be rate-limited
    UP_LIM:     REAL;           //Per-scan increase limit
    DN_LIM:     REAL;           //Per-scan decrease limit
    PASS_THRU: 	BOOL := FALSE;  // if true, OUT is set to IN.
END_VAR

VAR_OUTPUT
    OUT:        REAL;           //Input value after limiting
END_VAR


    
IF(IN > OUT) THEN
	IF(ABS(IN - OUT) > UP_LIM) THEN
		OUT := OUT + UP_LIM;
	ELSE
		OUT := IN;
	END_IF;
END_IF;

IF(IN < OUT) THEN
	IF(ABS(IN - OUT) > DN_LIM) THEN
		OUT := OUT - DN_LIM;
	ELSE
		OUT := IN;
	END_IF;
END_IF;


IF (PASS_THRU) THEN
	OUT := IN;
END_IF

