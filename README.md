# ST_Misc
Miscellaneous process control utility blocks, written in IEC 61131-3 structured text, 
released under an MIT license.

I've often found the standard blocks lacking
some handy features, e.g. bumpless switching. Over the years, these are the tricks
I've accummulated. I've put them here so others can benefit.

# Portability
For portability, I've avoided vendor-specific data types, relying on standard
IEC 61131-3 types.
Most of these have been tested on SEL RTAC controllers (3530), and some on Siemens
Step7. I've used the filename `<file>_SEL.st` or `<file>_s7.st` to note the 
differences where it gives a different file structure.
