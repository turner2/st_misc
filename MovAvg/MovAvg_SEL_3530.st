FUNCTION_BLOCK MovAvg
// VERSION : '0.1.1'
// AUTHOR  : 'JasonTurner jason@turner2.com.au'
// PLATFORM : Tested on SEL 3530.
// Moving Average. 
// Collect values, store in an array, and calculate the moving average.
// Set 'READY' to true when the array first fills.
//
// TODO: variable n_buckets, up to, say, 100?
//
// History:  
// v0.1.1 13 Oct 2017 Jason Turner.  Ported to 3530. Tested.
// v0.1 18 Jun 2016.  Jason Turner.  Initial version. Written for Step7 but not tested.

//*****************************************************
//MIT License
//
//Copyright (c) 2016 Jason Turner https://www.turner2.com.au
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.
//******************************************************/


VAR_INPUT
	IN: REAL;  // input to be filtered.
	INIT: BOOL := FALSE; //Re-start averaging.
END_VAR
	
VAR_OUTPUT
	MAVG: REAL;  //result of filter
	READY: BOOL := FALSE;  // filter has sampled at least max_n buckets.
END_VAR

VAR
	sum: REAL := 0.0;
	ii: INT := 1;
	max_n: INT := 20; //set to how many buckets you need.
	buckets: ARRAY[1..20] OF REAL;  //set the upper count to how many buckets you need.
END_VAR

BEGIN

	IF INIT = TRUE THEN
		ii := 1;
		READY := FALSE;
		sum := 0.0;
	END_IF

	IF READY = TRUE THEN // Normal operation
		sum := sum + IN - buckets[ii];		
		MAVG := sum / max_n;
	ELSE //block initializing - not yet done filling the buckets
		sum := sum + IN;
		MAVG := sum / ii;
	END_IF
	
	buckets[ii] := IN;
	ii := ii + 1;

	IF ii > max_n THEN 
		ii := 1;
		READY := TRUE;
	END_IF

END_FUNCTION_BLOCK
