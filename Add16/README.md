# Add16

Selectively add up to 16 `Real` inputs.

Block takes up to 16 `Real` inputs (`IN_n`, where n = 1, 2, ... , 16). For each `IN_n`, there
is a corresponding `USE_n`. If `USE_n` is true, the value of `IN_n` is added to the total, `OUT`.
The output `N` is the number of inputs that are used.

The original intention of `USE_n` was to remove inputs from the total on bad signal quality.

If all 16 inputs have `USE_n` false, `OUT` will not update, and `N` will be zero.